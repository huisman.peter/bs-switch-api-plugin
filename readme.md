# Browser sync Switch API plugin

Switch  server response

```
$ npm install browser-sync --save-dev
$ npm install bs-switch-api-plugin --save-dev
```

## Starting browser sync

``` javascript
var browserSync = require('browser-sync').create();

browserSync.init({
    server: './',
    plugins: [require('bs-switch-api-plugin')]
})

```
## Using browser sync switch api plugin

Create folder api

Add some json files in api folder

response1.json
```
{
  "routes": [
    {
      "/sample": {
        "GET": {
          "response": {
            "headers": [
            ],
            "status": 200,
            "body": {
              "Token": "!ThisIsAValidToken!",
              "UserName": "Peter Huisman",
              "CustomerID": "1234"
            }
          }
        }
      }
    },

    {"/sample":
      {
        "OPTIONS" : {
          "response" : {
            "headers" : [
            ],
            "status" : 200
          }
        }
      }
    }
  ]
}
```

response2.json
```
{{
  "routes": [
    {"/sample":
      {
        "OPTIONS" : {
          "response" : {
            "headers" : [
            ],
            "status" : 200
          }
        }
      }
    },
    {"/sample":
      {
        "GET" : {
          "response" : {
            "headers" : [
            ],
            "status" : 400,
            "body" : {
              "error" : "Ongeldige gebruikersnaam en of wachtwoord"
            }
          }
        }
      }
    }
  ]
}
```


Start browserSync

In your browser press ctrl+shift A

Select the server response from the dropdown at the bottom of your browser screen.
