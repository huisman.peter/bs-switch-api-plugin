const browserSync = require('browser-sync').create();

browserSync.init({
    open: false,
    server: './',
    plugins: [require('./')]
})
